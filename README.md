# Halting Problem

This is my version of [Sebastian Beyer's version](https://github.com/sebastianbeyer/KOSMO_haltingproblem) of [Ben Rufenacht's version](https://github.com/BenRufenacht/KosmoTuringMachine) of the [Turing Machine Eurorack module](https://github.com/TomWhitwell/TuringMachine) by Tom Whitwell.

Here is the description of the module from Tom Whitwell:

----

Since it was launched in June 2012, the Turing Machine has become one of the most popular Eurorack DIY projects. It is a random looping sequencer that spits out basslines and melodies. It generates strings of random voltages that can be locked into looping sequences. 
These sequences can be allowed to slip, changing gradually over time. This module was inspired by the long history of shift register pseudorandom synth circuits, including the Triadex Muse, Buchla 266 Source of Uncertainly and Grant Richter’s Noisering.

[This video explains how the Turing Machine works.](https://www.youtube.com/watch?v=Le26BIqB8Y8)  

The 2016 revised Turing Machine has many improvements:

- Rotary loop length switch 
- Pulse out
- Two boards, includes Backpack circuit to drive expanders (compatible with old Music Thing expanders)  
- Easier to build: a larger PCB in the same width, clock and noise circuits are more robust
- Noise level trimmer & reduced voltage in noise circuit to make circuit more robust with different noise transistors 
- Compatible with existing expanders 


[Original schematic](https://github.com/TomWhitwell/TuringMachine/blob/master/Collateral/PDF%20Schematics/TuringMachine2_Schematic_May2016.pdf)

License:
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/)

----

## Design changes:

Rufenacht converted Whitwell's design to Kosmo format and renamed it Halting Problem, Beyer revised Rufenacht's controls layout.

My changes to Beyer's version are mostly stylistic:

* Merged jacks board into main KiCad project
* Panel converted to AO style
* Bottom LEDs repositioned
* Changed to slotted hole footprints for jacks
* Changed LED resistor values to "RL"
* Changed PCB and panel labels

More substantively (though still minor):

* Changed beads to Schottkys
* Added Schottky on CD4016 input

I'm not a fan of ferrite beads on synth module power rails (see [this article from North Coast Synthesis](https://northcoastsynthesis.com/news/the-truth-about-ferrite-beads-will-shock-you/)). As for the CD4016, the TL072 is wired as a comparator so outputs ~±10.5 V, while the CD4016's logic inputs are supposed to be in the range -0.5 V to +12.5 V. They have CMOS protection diode networks that, given the series resistor between the op amp and the CD4016, will limit the pin voltage to about -0.6 V, and these networks should be able to handle currents of up to about 10 mA; here the maximum current would be about 100 µA. So maybe there really isn't a problem, but using the protection network as a circuit design element still bugs me, and there was space on the board for a Schottky to ground that eliminates the issue.

There are a lot of LEDs here, 10 of them to be precise. Taking Vf ~ 3 V, a 2.2kΩ series resistor and a supply voltage of 12 V means about 4 mA current per LED — if half of them are lit that's ~20 mA but it varies as the LEDs light up or not. That kind of varying load can put some noticeable fluctuations on the power rail. Modern super bright LEDs are too blinding to use at the same current, but instead of "super bright" you can consider them "super low current", providing equal illumination at much smaller current; I've found, for instance, green LEDs that work well at 12 V with a series resistance of 150kΩ, for a current draw of around 60 µA per LED. Using "RL" for the resistor values makes clear which footprints are affected by a change to brighter LEDs (or a difference in individual brightness preference).

## Current draw
26 mA +12 V, 18 mA -12 V

## Photos

![Halting Problem completed module](Images/HaltingProblem.jpg)

## Documentation

* [My schematic](Docs/TuringMain.pdf)
* PCB layout: [front](Docs/TuringMain_layout_front.pdf), [back](Docs/TuringMain_layout_back.pdf)
* [BOM](Docs/TuringMain_bom.md)

## Git repository

* [https://gitlab.com/rsholmes/KosmoTuringMachine](https://gitlab.com/rsholmes/KosmoTuringMachine)


