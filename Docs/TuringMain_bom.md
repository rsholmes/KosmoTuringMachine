# TuringMain.kicad_sch BOM

Sun 27 Nov 2022 02:35:03 PM EST

Generated from schematic by Eeschema 6.0.9-8da3e8f707~117~ubuntu22.04.1

**Component Count:** 111

| Refs | Qty | Component | Description | Manufacturer | Part | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- | ---- |
| AND_1, AND_2 | 2 | 4081 |  |  |  |  |  |
| BUFFER1, BUFFER2 | 2 | 4050 |  |  |  |  |  |
| C1, C5, C6, C7, C8, C13 | 6 | 1nF | Polyester film capacitor, 5 mm pitch |  |  |  |  |
| C2 | 1 | 10nF | Polyester film capacitor, 5 mm pitch |  |  |  |  |
| C3, C4, C15, C16, C17, C18, C19, C20, C21, C23, C24, C25, C26 | 13 | 100nF | Ceramic capacitor, 2.5 mm pitch |  |  |  |  |
| C9, C10, C12 | 3 | 0.47uF | Polyester film capacitor, 5 mm pitch |  |  |  |  |
| C11 | 1 | 330pF | Ceramic capacitor, 5 mm pitch |  |  |  |  |
| C14 | 1 | 100nF | Polyester film capacitor, 5 mm pitch |  |  |  |  |
| C22, C27 | 2 | 10uF | Electrolytic capacitor, 2.5 mm pitch |  |  | Tayda |  |
| CHANGE1, SCALE1 | 2 | 50k | Potentiometer |  |  | Tayda |  |
| CLK1, PULS1, UNK1, UNK2, UNK3, UNK4, UNK5, UNK6, UNK7, UNK8 | 10 | LED5MM | Light emitting diode |  |  | Tayda | A-1553 |
| CLOCK1, CV_IN1, CV_OUT1, NOISE-OUT1, PULSE_OUT1 | 5 | THONKICONNNEW | Audio Jack, 2 Poles (Mono / TS) |  |  | Tayda | A-1121 |
| D1 | 1 | 1N4148 |  |  |  |  |  |
| D2, D3, D4 | 3 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 |  |  | Tayda | A-159 |
| GATES1 | 1 | MA08-2 | Generic connector, double row, 02x08, odd/even pin numbering scheme (row 1 odd numbers, row 2 even numbers), script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |  |  |
| IC1 | 1 | TL072P |  |  |  |  |  |
| IC2, IC3 | 2 | 78L09 | Positive 100mA 30V Linear Regulator, Fixed Output 9V, TO-92 |  |  | Tayda | A-878 |
| J1 | 1 | Conn_01x10_Female | Generic connector, single row, 01x10, script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |  |  |
| J2 | 1 | Conn_01x10_Male | Generic connector, single row, 01x10, script generated (kicad-library-utils/schlib/autogen/connector/) |  |  |  |  |
| LENGTH1 | 1 | ALPS-SRBV | Rotary switch, PCB mount 8 position | Alpha | SR1712F-0108-20F0A-N9-N-027 | Mouser | 105-SR1712F-18NS |
| PULSES1 | 1 | MA08-2 |  |  |  |  |  |
| R1, R6, R7, R8, R12, R13, R14, R17, R23, R27 | 10 | 10k | Resistor |  |  | Tayda |  |
| R2 | 1 | 3.3k | Resistor |  |  | Tayda |  |
| R3, R5, R21, R24, R25 | 5 | 100k | Resistor |  |  | Tayda |  |
| R4, R9 | 2 | 1.6k | Resistor |  |  | Tayda |  |
| R10 | 1 | 51k | Resistor |  |  | Tayda |  |
| R11 | 1 | 47k | Resistor |  |  | Tayda |  |
| R15, R22, R26, R30, R39 | 5 | 1k | Resistor |  |  | Tayda |  |
| R16 | 1 | 5.1k | Resistor |  |  | Tayda |  |
| R18 | 1 | 68k | Resistor |  |  | Tayda |  |
| R19, R29 | 2 | 470k | Resistor |  |  | Tayda |  |
| R20, R28 | 2 | 15k | Resistor |  |  | Tayda |  |
| R31, R32, R33, R34, R35, R36, R37, R38, R40, R41 | 10 | RL | Resistor |  |  | Tayda |  |
| SHIFTREG_EXT1, SHIFTREG_MAIN1 | 2 | 4015 |  |  |  | Digi-Key | 296-2035-5-ND |
| SV1 | 1 | 10_PIN_HEADER | Pin header 2.54 mm 2x5 |  |  | Tayda | A-2939 |
| SWITCH_IC1 | 1 | 4016 |  |  |  | Digi-Key | 296-2036-ND |
| T1 | 1 | 2N3904 | Small Signal NPN Transistor, TO-92 |  |  | Tayda | A-111 |
| TL1 | 1 | TL431CLP | Shunt Regulator, TO-92 |  |  |  |  |
| U1 | 1 | TL074 |  |  |  |  |  |
| U$1 | 1 | DAC0800 |  |  |  | Digi-Key | DAC0808LCN/NOPB-ND |
| VR1 | 1 | 1M |  |  |  |  |  |
| WRITE1 | 1 | T-80-T-SPDT-ON-OFF-ON |  |  |  |  |  |
    
